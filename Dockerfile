FROM alpine:latest
COPY ./bin/* /app/
WORKDIR /app
ENTRYPOINT ["/app/slackchatgpt"]