# What is this?

In short, it's an abstraction of the ChatGPT API, specifically to be installed in a Slack instance.

## To Run...
* Locally: `make run`
* In Docker: `make docker`, with `API_KEY` and `PORT` defined as envvars
* In K8s/K3s: `kubectl -f slackchatgpt.yaml`, changing things in the Yaml file to your liking.

...then...
* Test locally: `curl http://127.0.0.1:5050 -F "text=joke" -F "response_url=blah"`
* Install as slashcommand in your Slack instance.