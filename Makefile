.PHONY: default
default: displayhelp ;

displayhelp:
	@echo Use "clean, showcoverage, tests, build, buildlinux or run" with make, por favor.

showcoverage: tests
	@echo Running Coverage output
	go tool cover -html=coverage.out

tests: clean
	@echo Running Tests
	go test --coverprofile=coverage.out ./...

docker: build
	docker build -t slackchatgpt:latest . -f Dockerfile
	docker run -it --env API_KEY=$$API_KEY --env PORT=$$PORT -p $$PORT:$$PORT slackchatgpt:latest

run: build
	@echo Running program
	./bin/slackchatgpt

build:
	@echo Running build command
	CGO_ENABLED=0 go build -o bin/slackchatgpt src/main.go

clean:
	@echo Removing binary TODO
	rm -rf ./bin ./vendor Gopkg.lock
