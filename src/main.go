package main

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	homedir "github.com/mitchellh/go-homedir"
	gogpt "github.com/sashabaranov/go-openai"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"time"
)

func initConfig() (err error) {
	log.Printf("Starting SlackChatGPT, getting config")
	// Find home directory.
	home, err := homedir.Dir()
	if err != nil {
		fmt.Println(err)
		return err
	}
	viper.AddConfigPath(home)
	viper.SetConfigType("toml")
	viper.AddConfigPath(".")
	viper.AddConfigPath("~")
	viper.SetConfigName("slackchatgpt.toml")
	pflag.Int("PORT", 5050, "This is the port to bind to. 5050 is default.")
	pflag.String("API_KEY", "", "This is the key to use for ChatGPT.")
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)
	viper.AutomaticEnv()
	err = viper.ReadInConfig()
	if err != nil {
		fmt.Println("Uh oh. no config file; will use envvars.")
		err = nil
	}
	fmt.Printf("Will use %d as port number.\n", viper.GetInt("port"))
	if viper.GetInt("port") == 0 {
		err = errors.New("No valid port number")
	}
	if viper.GetString("api_key") == "" {
		err = errors.New("No API_KEY found.")
	}
	return err
}
func GetGPT(prompt string) (string, error) {
	c := gogpt.NewClient(viper.GetString("api_key"))
	resp, err := c.CreateChatCompletion(
		context.Background(),
		gogpt.ChatCompletionRequest{
			Model: gogpt.GPT3Dot5Turbo,
			Messages: []gogpt.ChatCompletionMessage{
				{
					Role:    gogpt.ChatMessageRoleUser,
					Content: prompt,
				},
			},
		},
	)
	if err != nil {
		fmt.Printf("err: %s", err.Error())
		return "", err
	}
	fmt.Printf("Response from GPT is: %s\n", resp.Choices[0].Message.Content)
	return resp.Choices[0].Message.Content, err
}
func AsyncHandle(prompt string, responseURL string) {
	text, err := GetGPT(prompt)

	reqBody, err := json.Marshal(map[string]string{
		"text":          text,
		"response_type": "in_channel",
	})
	client := &http.Client{
		Timeout: time.Second * 10,
	}
	req, err := http.NewRequest("POST", responseURL, bytes.NewReader(reqBody))
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		print(err)
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		print(err)
	}
	fmt.Println(string(body))
}
func RootHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		response := fmt.Sprintf("Verb %s not implemented.", r.Method)
		http.Error(w, response, http.StatusInternalServerError)
		return
	}
	prompt := r.FormValue("text")
	fmt.Printf("Prompt is: %s.\n", prompt)
	responseURL := r.FormValue("response_url")
	fmt.Printf("ResponseURL is: %s\n", responseURL)

	if responseURL != "" {
		//return immediate 200 now.
		data, _ := json.Marshal(map[string]string{
			"response_type": "in_channel",
			"text":          fmt.Sprintf("%s", "(:shushing_face: Hang tight; I'm doing some async things to get around Slack's 3 second return rule for POST's, since ChatGPT takes a bit of time.....)"),
		})
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		w.Write(data)
		go AsyncHandle(prompt, responseURL)
	} else {
		//go do things
		text, err := GetGPT(prompt)
		if err == nil {
			data, _ := json.Marshal(map[string]string{
				"response_type": "in_channel",
				"text":          fmt.Sprintf("%s", text),
			})
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(200)
			w.Write(data)

		} else {
			data, _ := json.Marshal(map[string]string{
				"response_type": "ephemeral",
				"text":          fmt.Sprintf("An error was encountered: %s", err.Error()),
			})
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(500)
			w.Write(data)
		}
	}
}
func handleLocalRequests() {
	http.HandleFunc("/", RootHandler)
	//http.HandleFunc("/other", OtherHandler)
	log.Printf("Started Webserver on port %d.\n", viper.GetInt("port"))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", viper.GetInt("port")), nil))
}

func main() {
	//fmt.Printf("Starting Webserver on port %s.\n", "5050")
	//http.HandleFunc("/", RootHandler)
	//http.ListenAndServe("0.0.0.0:5050", nil)
	rand.Seed(time.Now().Unix())
	err := initConfig()
	if err == nil {
		log.Printf("Initialized...")
		handleLocalRequests()
	} else {
		log.Fatalf("Error initing app, will exit: %s\n", err.Error())
	}

}
